# ZGo - The Zcash Register

A Point-of-Sale application for accepting payments in [Zcash](https://z.cash/)

Visit our [ZGo Homepage](https://zgo.cash/) for more details.
