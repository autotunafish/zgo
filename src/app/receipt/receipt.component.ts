import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Order} from '../order/order.model';
import { ReceiptService } from '../receipt.service';
import { Observable }  from 'rxjs';

@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.component.html',
  styleUrls: ['./receipt.component.css']
})
export class ReceiptComponent implements OnInit {
	orderId;
	public orderUpdate: Observable<Order>;
	public nameUpdate: Observable<string>;
	name: string = '';
	error: boolean = false;
	order:Order = {
			address: '',
			session: '',
			timestamp: '',
			closed: false,
			currency: '',
			price: 0,
			total: 0,
			totalZec: 0,
			paid: false,
			externalInvoice: '',
			shortCode: '',
			lines: [
				{
					qty: 1,
					name: '',
					cost:0
				}
			]
		};

  constructor(
	  private _ActiveRoute:ActivatedRoute,
	  public receiptService: ReceiptService
  ) {
	  this.orderId = this._ActiveRoute.snapshot.paramMap.get("orderId");
	  this.orderUpdate = receiptService.orderUpdate;
	  this.nameUpdate = receiptService.nameUpdate;
	  receiptService.getOrderById(this.orderId!);
	  this.orderUpdate.subscribe(order => {
		  this.order = order;
	  });

	  this.nameUpdate.subscribe(name => {
		  this.name = name;
	  });
  }

  ngOnInit(): void {
  }

}
