import { Input, Inject, Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig} from '@angular/material/dialog';

import { faCartShopping } from '@fortawesome/free-solid-svg-icons';

import { Observable } from 'rxjs';
import { Item } from '../item.model';
import { Owner } from '../../owner.model';
import { FullnodeService } from '../../fullnode.service';
import { UserService } from '../../user.service';
import { ItemService } from '../items.service';
import { OrderService} from '../../order/order.service';
import { ItemCreateComponent } from '../item-create/item-create.component';
import { ItemEditComponent } from '../item-edit/item-edit.component';
import { ItemDeleteComponent } from '../item-delete/item-delete.component';
import { ItemAddComponent } from '../item-add/item-add.component';
import { NotifierService } from '../../notifier.service';

@Component({
	selector: 'app-item-list',
	templateUrl: './item-list.component.html',
	styleUrls: ['./item-list.component.css']
})

export class ItemListComponent implements OnInit{

	@Input() zecPrice: number;

	public items: Item[] = [];

	faCartShopping = faCartShopping;
    
	owner: Owner = {
		_id: '',
		name: '',
		address: '',
		currency: 'usd',
		tax: false,
		taxValue: 0,
		vat: false,
		vatValue: 0,
		first: '',
		last: '',
		email: '',
		street: '',
		city: '',
		state: '',
		postal: '',
		phone: '',
		paid: false,
		website: '',
		country: '',
		zats: false,
		invoices: false,
		expiration: new Date(Date.now()).toISOString(),
		payconf: false,
		viewkey: '',
		crmToken: ''
	};

	public ownerUpdate: Observable<Owner>;
	public itemsUpdate: Observable<Item[]>;

	constructor(
		public itemService: ItemService,
		userService: UserService,
		public orderService: OrderService,
		public fullnodeService: FullnodeService,
		private dialog: MatDialog,
		private notifierService: NotifierService )
	{
			this.ownerUpdate = userService.ownerUpdate;
			this.itemsUpdate = itemService.itemsUpdated;
			this.ownerUpdate.subscribe((owner) => {
				this.owner = owner;
				itemService.getItems(this.owner.address);
				this.itemsUpdate.subscribe((items) => {
					this.items = items;
				});
			});
			this.zecPrice = 1;
	}

	ngOnInit(){
		this.itemsUpdate.subscribe((items) => {
			this.items = items;
		});
	}

	openDialog(){
		const dialogConfig = new MatDialogConfig();

		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.data = {_id: '', name: '' , user: '', description: '', cost: 0}

		const dialogRef = this.dialog.open(ItemCreateComponent, dialogConfig);

		dialogRef.afterClosed().subscribe((val) => {
			if(val != null) {
				var item:Item = {_id: '', name: val.name, description: val.description, cost: val.cost, owner: this.owner.address};
				this.itemService.addItem(item);
				this.notifierService
				    .showNotification("Item successfully created!",
			    	              "Close","success");
			}
			this.itemService.getItems(this.owner.address);
			this.itemsUpdate.subscribe((items) => {
				this.items = items;
			});
		});
	}

	edit(id: string) {
//		console.log('Edit:', id);
		const item = this.items.find(element => element._id == id);
//		console.log(item);
		const dialogConfig = new MatDialogConfig();

		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.data = item;

		const dialogRef = this.dialog.open(ItemEditComponent, dialogConfig);
		dialogRef.afterClosed().subscribe((val) => {
			if (val != null) {
				var editItem: Item = {
					_id: val.id,
					name: val.name,
					description: val.description,
					cost: val.cost,
					owner: this.owner.address
				};
				//console.log('Edit:', editItem);
				this.itemService.addItem(editItem).subscribe((response) => {
					this.itemService.getItems(this.owner.address);
				});
				this.notifierService
				    .showNotification("Item information updated!!",
				    	              "Close","success");
			}
			this.itemService.getItems(this.owner.address);
		});
	}

	delete(id: string) {
		//console.log('Edit:', id);
		const item = this.items.find(element => element._id == id);
		//console.log(item);
		const dialogConfig = new MatDialogConfig();

		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.data = item;

		const dialogRef = this.dialog.open(ItemDeleteComponent, dialogConfig);
		dialogRef.afterClosed().subscribe((val) => {
			if (val != null) {
//				console.log('Deleting', val);
				this.itemService.deleteItem(val);
				this.items = [];
				this.notifierService
				    .showNotification("Item deleted!!",
			    	              "Close","success");
			}
			this.itemService.getItems(this.owner.address);
			this.itemsUpdate.subscribe((items) => {
				this.items = items;
			});
		});
	}

	addToOrder(id: string) {
		const item = this.items.find(element => element._id == id);
		//console.log(item);
		const dialogConfig = new MatDialogConfig();

		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.data = {
			qty: 1,
			name: item!.name,
			cost: item!.cost
		};

		const dialogRef = this.dialog.open(ItemAddComponent, dialogConfig);
		dialogRef.afterClosed().subscribe((val) => {
			if (val != null) {
				console.log('Adding to order', val);
				this.orderService.addToOrder(val);
			}
			this.itemService.getItems(this.owner.address);
		});
	}

	getCurrency(){
		return this.owner.currency.toUpperCase();
	}
}

