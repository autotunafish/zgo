export interface Item {
	_id?: string;
	name: string;
	description: string;
	cost: number;
	owner: string;
}
