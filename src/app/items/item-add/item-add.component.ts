import { Inject, Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UntypedFormBuilder, Validators, UntypedFormGroup, FormControl } from '@angular/forms';
import { LineItem } from '../lineitem.model';
import { Order } from '../../order/order.model';

@Component({
	selector: 'app-item-add',
	templateUrl: './item-add.component.html',
	styleUrls: ['./item-add.component.css']
})

export class ItemAddComponent implements OnInit {

	orderForm: UntypedFormGroup;
	lineItem: LineItem;
	//order: Order;

	constructor(
		private fb: UntypedFormBuilder,
		private dialogRef: MatDialogRef<ItemAddComponent>,
		@Inject(MAT_DIALOG_DATA) public data: LineItem
	) {
		this.orderForm = fb.group({
			qty: [data.qty, Validators.required]
		});
		this.lineItem = {
			qty : data.qty,
			name : data.name,
			cost : data.cost
		}
	}

	ngOnInit() {
	}

	close() {
		this.dialogRef.close();
	}

	save() {
		this.lineItem.qty = this.orderForm.value.qty;
		this.dialogRef.close(this.lineItem);
	}
}
