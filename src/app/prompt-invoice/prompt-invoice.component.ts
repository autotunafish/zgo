import { Inject, Component, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { NotifierService } from '../notifier.service';
import { faCopy } from '@fortawesome/free-solid-svg-icons';

var URLSafeBase64 = require('urlsafe-base64');
var Buffer = require('buffer/').Buffer;

@Component({
  selector: 'app-prompt-invoice',
  templateUrl: './prompt-invoice.component.html',
  styleUrls: ['./prompt-invoice.component.css']
})

export class PromptInvoiceComponent implements OnInit {
	orderId: string;
	invoiceUrl: string;
 
 	// ------------------------------------
	//
	faCopy = faCopy;
 	// ------------------------------------

	constructor(
			private dialogRef: MatDialogRef<PromptInvoiceComponent>,
			@Inject(MAT_DIALOG_DATA) public data: {orderId: string},
			private notifierService : NotifierService ) {
		this.orderId = data.orderId;
		this.invoiceUrl = 'https://app.zgo.cash/invoice/'+this.orderId;
	}

	ngOnInit(): void {
	}


	confirm() {
		this.dialogRef.close(true);
	}

	close() {
		this.dialogRef.close(false);
	}

	copyUrl() {
//		console.log("Inside copyUrl()");
		if (navigator.clipboard) {
		};
		try {
			navigator.clipboard.writeText(this.invoiceUrl);
			this.notifierService
			    .showNotification("Invoice's URL copied to Clipboard!!","Close",'success');

		} catch (err) {
//			console.error("Error", err);  
			this.notifierService
			    .showNotification("Functionality not available for your browser. Use send button instead.","Close",'error');
		}
	}
}
