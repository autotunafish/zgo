import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromptInvoiceComponent } from './prompt-invoice.component';

describe('PromptInvoiceComponent', () => {
  let component: PromptInvoiceComponent;
  let fixture: ComponentFixture<PromptInvoiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromptInvoiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromptInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
