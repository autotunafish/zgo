import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

	constructor(
		private dialogRef: MatDialogRef<TermsComponent>
	) { }

	ngOnInit(): void {
	}

	close() {
		this.dialogRef.close(false);
	}

}
