export interface Country {
	_id?: string;
	name: string;
	code: string;
}
