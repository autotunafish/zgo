import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromptReceiptComponent } from './prompt-receipt.component';

describe('PromptReceiptComponent', () => {
  let component: PromptReceiptComponent;
  let fixture: ComponentFixture<PromptReceiptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromptReceiptComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromptReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
