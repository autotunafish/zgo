import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from '../order/order.model';
import { FullnodeService } from '../fullnode.service';
import { UserService } from '../user.service';
import { Owner } from '../owner.model';
import { OrderService } from '../order/order.service';

import { MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { PromptInvoiceComponent } from '../prompt-invoice/prompt-invoice.component';
import { PromptReceiptComponent } from '../prompt-receipt/prompt-receipt.component';
import { DbExportComponent } from '../db-export/db-export.component';

import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { faHourglass } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
	selector: 'app-list-orders',
	templateUrl: './listorders.component.html',
	styleUrls: ['./listorders.component.css']
})

export class ListOrdersComponent implements OnInit, OnDestroy{
//	orderId;
	public todayTotal: number = 0;
	public total: number = 0;
	public orders: Order[] = [];
	public ownerUpdate: Observable<Owner>;
	public ordersUpdate: Observable<Order[]>;
    

	// ------------------------------------
	//
	faTimes = faTimes;
	faTimesCircle = faTimesCircle;
	faCheck = faCheck;
	faCheckCircle = faCheckCircle;
	faHourglass = faHourglass;
	faTrash = faTrash;
	payConf : boolean = false;
	owner : Owner = {
			address: '',
			name: '',
			currency: 'usd',
			tax: false,
			taxValue: 0,
			vat: false,
			vatValue: 0,
			first: '',
			last: '',
			email: '',
			street: '',
			city: '',
			state: '',
			postal: '',
			phone: '',
			paid: false,
			website: '',
			country: '',
			zats: false,
			invoices: false,
			expiration: new Date(Date.now()).toISOString(),
			payconf: false,
			viewkey: '',
			crmToken: ''
		};
	// -------------------------------------


	constructor(
		public orderService: OrderService,
		public userService: UserService,
		private dialog: MatDialog)
	{
		this.ownerUpdate = userService.ownerUpdate;
		this.orderService.getAllOrders();
		this.ordersUpdate = orderService.allOrdersUpdate;
	}

	ngOnInit(){
//		console.log('listOrders Init -->');
		this.owner = this.userService.currentOwner();
//        console.log(this.owner.name);
        this.payConf = this.owner.payconf;
//        this.payConf = true;
//        console.log('payConf = ', this.payConf);

		this.ordersUpdate.subscribe((orders) => {
			this.total = 0;
			this.todayTotal = 0;
			var today = new Date();
			this.orders = orders;
	
			console.log(this.ownerUpdate);
			for (let i=0; i < this.orders.length; i++){
				this.total += this.orders[i].totalZec;
			//

				var date = new Date(this.orders[i]!.timestamp!);
				var diff = (today.getTime() / 1000) - (date.getTime()/1000);
				if (diff < (24*3600)){
					this.todayTotal += this.orders[i].totalZec;
				}
			}
		});
	}

	ngOnDestroy(){
		this.total = 0;
		this.todayTotal = 0;
	}

	getIcon(order : Order) {
	    if( order.paid )
		  return faCheckCircle;
		return faHourglass;
	}

	getIconStyle(order : Order) {
		if( order.paid )
		  return "font-size: 14px; color: #72cc50; margin-bottom: -2px;";
		return "color: #FB4F14; margin-bottom: -2px; cursor: pointer;";

	}

	invoice(order : Order) {
//		var zec = this.total/this.price;
//		this.order.totalZec = parseFloat(zec.toFixed(8));
		const dialogConfig = new MatDialogConfig();
		
//		console.log("Order data:");
//		console.log(order);
//		console.log("order.total = " + order.total);

		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.data = {
			orderId: order._id
		};

		const dialogRef = this.dialog.open(PromptInvoiceComponent, dialogConfig);
		dialogRef.afterClosed().subscribe((val) => {
				console.log('Returning to order list');
		});
	}

	receipt(order : Order) {
//		var zec = this.total/this.price;
//		this.order.totalZec = parseFloat(zec.toFixed(8));
		const dialogConfig = new MatDialogConfig();
		
//		console.log("Order data:");
//		console.log(order);
//		console.log("order.total = " + order.total);

		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.data = {
			orderId: order._id
		};

		const dialogRef = this.dialog.open(PromptReceiptComponent, dialogConfig);
		dialogRef.afterClosed().subscribe((val) => {
				console.log('Returning to order list');
		});
	}

	openDbExport(){
		const dialogConfig = new MatDialogConfig();

		console.log('openDbExport ---');

		dialogConfig.disableClose = false;
		dialogConfig.autoFocus = true;
		dialogConfig.data = this.owner;

		const dialogRef = this.dialog.open(DbExportComponent, dialogConfig);
		dialogRef.afterClosed().subscribe((val) => {
				console.log('Returning to order list');
		});

	}

}
