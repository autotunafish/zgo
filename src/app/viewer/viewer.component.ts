import { Component, OnInit, OnDestroy } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UserService } from '../user.service';
import { FullnodeService } from '../fullnode.service';
import { ItemService } from '../items/items.service';
import { Subscription, Observable } from 'rxjs';
import { SettingsComponent } from '../settings/settings.component';

import {Owner} from '../owner.model';
import {User} from '../user.model';


@Component({
	selector: 'app-viewer',
	templateUrl: './viewer.component.html',
	styleUrls: ['./viewer.component.css']
})

export class ViewerComponent implements OnInit {
	//intervalHolder: any;
	public message: string = "Welcome to the inside!";
	public user: User = {
		address: '',
		session: '',
		blocktime: 0,
		pin: '',
		validated: false
	};
	private owner: Owner= {
		_id:'',
		address: 'none',
		name:'',
		currency: 'usd',
		tax: false,
		taxValue: 0,
		vat: false,
		vatValue: 0,
		first: '',
		last: '',
		email: '',
		street: '',
		city: '',
		state: '',
		postal: '',
		phone: '',
		paid: false,
		website: '',
		country: '',
		zats: false,
		invoices: false,
		expiration:  new Date(Date.now()).toISOString(),
		payconf: false,
		viewkey: '',
		crmToken: ''
	};
	public price: number = 1;
	public addrUpdate: Observable<string>;
	public ownerUpdate: Observable<Owner>;
	public userUpdate: Observable<User>;
	public priceUpdate: Observable<number>;
	orientation: boolean = false;

	constructor(
		public fullnodeService: FullnodeService,
		private router: Router,
		public userService: UserService,
		private dialog: MatDialog
	){
		this.addrUpdate = fullnodeService.addrUpdate;
		this.ownerUpdate = userService.ownerUpdate;
		this.priceUpdate = fullnodeService.priceUpdate;
		this.ownerUpdate.subscribe((owner) => {
			this.owner = owner;
		});

//		console.log(this.owner._id);

		this.userUpdate = userService.userUpdate;
		this.userUpdate.subscribe((user) => {
			this.user = user;
		});
		this.priceUpdate.subscribe((price) => {
			this.price = price;
		});
	}

	ngOnInit(){
		this.orientation = (window.innerWidth <= 500);
		this.ownerUpdate.subscribe((owner) => {
			this.message = owner.name;
		});
		this.loginCheck();
		//this.intervalHolder = setInterval(() => {
			//this.loginCheck();
		//}, 60000);
	}

	ngOnDestroy(){
	}

	onResize(event: any){
		this.orientation = (event.target.innerWidth <= 500);
	}

	shortenZaddr(address:string) {
		var addr = address;
		var end = addr.length;
		var last = end - 5;
		return addr.substring(0,5).concat('...').concat(addr.substring(last, end));
	}

	openSettings() {

		const dialogConfig = new MatDialogConfig();

		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.data = this.owner;

		const dialogRef = this.dialog.open(SettingsComponent, dialogConfig);
		dialogRef.afterClosed().subscribe((val) => {
			if (val != null) {
				//console.log('Saving settings', val);
				this.userService.addOwner(val);
				this.fullnodeService.getPrice(val.currency);
				this.loginCheck();
			}
		});
	}

	loginCheck(){
		this.userService.findUser();
		this.ownerUpdate.subscribe((owner) => {
			this.owner = owner;
			this.userUpdate.subscribe((user) => {
				this.user = user;
				//console.log('Viewer loginCheck', this.user);
				if (!this.owner.paid || !this.user.validated) {
					console.log('Log in expired!');
					this.router.navigate(['/login']);
				}
			});
		});
	}
}
