import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatExpansionModule } from '@angular/material/expansion';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ViewerComponent } from './viewer/viewer.component';
import { LoginComponent } from './login/login.component';
import { ItemListComponent } from './items/item-list/item-list.component';
import { ItemCreateComponent } from './items/item-create/item-create.component';
import { ItemEditComponent } from './items/item-edit/item-edit.component';
import { ItemDeleteComponent } from './items/item-delete/item-delete.component';
import { ItemAddComponent} from './items/item-add/item-add.component';
import { OrderComponent } from './order/order.component';
import { CancelComponent } from './cancel/cancel.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { SettingsComponent } from './settings/settings.component';
import { ListOrdersComponent } from './listorders/listorders.component';
import { ScanComponent } from './scan/scan.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BusinessComponent } from './business/business.component';
import { SearchOptionsPipe } from './searchoptions.pipe';
import { TermsComponent } from './terms/terms.component';
import { ReceiptComponent } from './receipt/receipt.component';
import { ReceiptQRComponent } from './receipt-qr/receipt-qr.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PromptInvoiceComponent } from './prompt-invoice/prompt-invoice.component';
import { PromptReceiptComponent } from './prompt-receipt/prompt-receipt.component';
import { NotifierComponent } from './notifier/notifier.component';
import { PmtserviceComponent } from './pmtservice/pmtservice.component';
import { XeroRegComponent } from './xeroreg/xeroreg.component';
import { DbExportComponent } from './db-export/db-export.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        ViewerComponent,
        ItemListComponent,
        LoginComponent,
        OrderComponent,
        ItemCreateComponent,
        ItemEditComponent,
        ItemDeleteComponent,
        ItemAddComponent,
        CancelComponent,
        CheckoutComponent,
        SettingsComponent,
        ScanComponent,
        ListOrdersComponent,
        BusinessComponent,
        SearchOptionsPipe,
        TermsComponent,
        ReceiptComponent,
        ReceiptQRComponent,
        InvoiceComponent,
        PromptInvoiceComponent,
        PromptReceiptComponent,
        NotifierComponent,
        PmtserviceComponent,
        XeroRegComponent,
        DbExportComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatIconModule,
        HttpClientModule,
        MatInputModule,
        MatCardModule,
        MatButtonModule,
        MatToolbarModule,
        MatExpansionModule,
        MatDialogModule,
        MatDividerModule,
        MatListModule,
        MatSelectModule,
        MatProgressBarModule,
        MatStepperModule,
        MatAutocompleteModule,
        MatSlideToggleModule,
        MatSnackBarModule,
		MatTabsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        BrowserAnimationsModule,
        FontAwesomeModule
    ],
    exports: [
        MatIconModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
