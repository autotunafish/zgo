import  {Component, OnInit, OnDestroy} from '@angular/core';
import { MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {FullnodeService} from '../fullnode.service';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { CancelComponent } from '../cancel/cancel.component';
import {Subscription, Observable} from 'rxjs';

import {Owner} from '../owner.model';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit, OnDestroy {

	public height = 0;
	private owner: Owner= {
		_id:'',
		address: 'none',
		name:'',
		currency: 'usd',
		tax: false,
		taxValue: 0,
		vat: false,
		vatValue: 0,
		first: '',
		last: '',
		email: '',
		street: '',
		city: '',
		state: '',
		postal: '',
		phone: '',
		paid: false,
		website: '',
		country: '',
		zats: false,
		invoices: false,
		expiration: new Date(Date.now()).toISOString(),
		payconf: false,
		viewkey: '',
		crmToken: ''
	};
	private session: string | null = '';
	public heightUpdate: Observable<number>;
	public ownerUpdate: Observable<Owner>;
	public uZaddrUpdate: Observable<string>;

	constructor(
		public fullnodeService: FullnodeService,
		public userService: UserService,
		private dialog: MatDialog,
		private router: Router
	){
		this.heightUpdate = fullnodeService.heightUpdate;
		this.uZaddrUpdate = userService.uZaddrUpdate;
		this.ownerUpdate = userService.ownerUpdate;
		this.ownerUpdate.subscribe((owner) => {
			this.owner = owner;
		});
	}


	ngOnInit(){
	}

	ngOnDestroy(){
	}

	getCurrency(){
		return this.owner.currency.toUpperCase();
	}

	logout(){
		const dialogConfig = new MatDialogConfig();

		dialogConfig.disableClose = true;
		dialogConfig.autoFocus = true;
		dialogConfig.data = {title: 'Logout?', msg: 'Are you sure you want to disconnect from ZGo? You will have to re-link your wallet via shielded memo.'};

		const dialogRef = this.dialog.open(CancelComponent, dialogConfig);
		dialogRef.afterClosed().subscribe(val => {
			if(val){
//				console.log('Logout!');
				this.userService.deleteUser().subscribe(UserResponse => {
					console.log('Rerouting');
					this.userService.findUser();
					this.router.navigate(['/login']);
				});
			}
		});
	}
}
