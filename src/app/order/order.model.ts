import { LineItem } from '../items/lineitem.model';

export interface Order {
	_id?: string,
	address: string,
	session: string,
	timestamp?: string,
	closed: boolean,
	currency: string,
	price?: number,
	total: number,
	totalZec: number,
	lines: LineItem[],
	paid: boolean,
	externalInvoice: string,
	shortCode: string
}
