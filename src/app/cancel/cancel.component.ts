import { Inject, Component, OnInit, ViewEncapsulation} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
	selector: 'app-cancel',
	templateUrl: './cancel.component.html',
	styleUrls: ['./cancel.component.css']
})

export class CancelComponent {
	title: string;
	msg: string;

	constructor(
		private dialogRef: MatDialogRef<CancelComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { title: string, msg: string}
	) {
		this.title = data.title;
		this.msg = data.msg;
	}

	confirm() {
		this.dialogRef.close(true);
	}

	close() {
		this.dialogRef.close(false);
	}
}
